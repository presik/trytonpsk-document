# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['DocumentTracking', 'TypeDocument']

_STATES = {
    'readonly': Eval('state') != 'draft',
}

PRIORITY = [
    ('low', 'Low'),
    ('middle', 'Middle'),
    ('high', 'High'),
]


class TypeDocument(ModelSQL, ModelView):
    "Type Document"
    __name__ = 'document.type_document'
    name = fields.Char('Name', required=True)


class DocumentTracking(Workflow, ModelSQL, ModelView):
    "Document Tracking"
    __name__ = 'document.tracking'
    number = fields.Char('Number', readonly=True)
    date = fields.Date('Date', states=_STATES)
    subject = fields.Char('Subject', states=_STATES)
    company = fields.Many2One('company.company', 'Company',
            states=_STATES, required=True)
    description = fields.Char('Description', required=True, states=_STATES)
    kind = fields.Many2One('document.type_document',
        'Kind', required=True, states=_STATES)
    priority = fields.Selection(PRIORITY, 'Priority', required=True,
            states=_STATES)
    type = fields.Selection([
            ('legal', 'Legal'),
            ('party', 'Party'),
            ('client', 'Client'),
            ], 'Type', required=True, states=_STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ('canceled', 'Canceled'),
        ], 'State', required=True, readonly=True)
    appends = fields.Char('Appends', states=_STATES)
    recipients = fields.Char('Recipients', states=_STATES)

    @classmethod
    def __setup__(cls):
        super(DocumentTracking, cls).__setup__()
        cls._order.insert(0, ('number', 'ASC'))
        cls._transitions |= set((
            ('draft', 'cancelled'),
            ('draft', 'done'),
            ('cancelled', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['done', 'draft']),
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
            'done': {
                'invisible': Eval('state') != 'draft',
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_type():
        return 'legal'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass
